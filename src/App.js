import './App.css';
import React, {useState} from "react";

const Complete = () => {
    return <p>Вы исчерпали все нажатия</p>
}

const Welcome = (props) => {
    const [count, setCount] = useState(0);

    return (<div>
        <h1>Привет, {props.name}, у вас есть {10 - count} нажатий на кнопку</h1>
        <p>Вы нажали на кнопку {count} раз</p>
        {count >= 10 ? <Complete/> : <button onClick={() => setCount(count + 1)}>
            Нажми на меня
        </button>}

    </div>)
}

class WelcomeByClass extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            count: 0, hover: false
        };

    }

    handleMouseIn() {
        this.setState({hover: true})
    }

    handleMouseOut() {
        this.setState({hover: false})
    }

    handleRightClick(e) {
        e.preventDefault();
        console.log('Правая кнопка мыши здесь не работает!')
    }

    handleMouse(e) {

    }

    render() {
        const tooltipStyle = {
            display: this.state.hover ? 'block' : 'none'
        }

        return (<div onContextMenu={(e) => this.handleRightClick(e)}>
            <h1>Привет, {this.props.name}, у вас есть {10 - this.state.count} нажатий на кнопку</h1>
            <p>Вы нажали на кнопку {this.state.count} раз</p>
            {this.state.count >= 10 ? <Complete/> : <button onClick={() => this.setState({count: this.state.count + 1})}
                                                            >
                Нажми на меня
            </button>}
            <button onClick={() => this.setState({count: 0})} onMouseOver={this.handleMouseIn.bind(this)}
                    onMouseOut={this.handleMouseOut.bind(this)}>Сбросить счётчик</button>
            <div>
                <div style={tooltipStyle}>При нажатии на эту кнопку, у вас снова будет 10 попыток для нажатия кнопки!</div>
            </div>
        </div>)
    }
}

const Header = () => {

    return <header className="App-header">
        <a
            className="App-link"
            href="https://reactjs.org"
            target="_blank"
            rel="noopener noreferrer"
        >
            Learn React
        </a>
    </header>;
}

function App() {
    return (<div className="App">
        <Header/>
        <Welcome name="Reviewer"/>
        <WelcomeByClass name="Reviewer"/>
    </div>);
}

export default App;
