# Lesson 1. GIT

Lesson 1. Home task

## Name
**Novikov Ivan**

# Lesson 2. Docker
Start project: `docker-compose build && docker-compose up`

# Lesson 3. Native JS

# Lesson 4. Simple React App
Run app 
`npm start`

# Lesson 5. Virtual DOM

# Lesson 6. React Components
Run app
`npm start`

# Lesson 7. State and lifecycle of class component

# Lesson 8. Handling events and conditional rendering

# Lesson 9. Lifting state up and forms

